import { InputType, Field } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';

@InputType()
export class CreateSupportMeasuresInput
  implements Prisma.Support_measuresCreateInput
{
  @Field(() => String)
  value: string;
}
