import { InputType, Field } from '@nestjs/graphql';
import { $Enums, Prisma } from '@prisma/client';
import { IsEmail, IsMobilePhone } from 'class-validator';

@InputType()
export class UpdateCitizenInput implements Prisma.CitizenUpdateInput {
  @Field({ nullable: true })
  first_name?: string;

  @Field({ nullable: true })
  last_name?: string;

  @Field({ nullable: true })
  second_name?: string;

  @Field({nullable: true})
  category?: $Enums.Category

  @Field({ nullable: true })
  organizationName?: string;

  @Field({ nullable: true })
  email?: string;

  @Field({ nullable: true })
  phone?: string;

  @Field({ nullable: true })
  user_bot_id?: string;
}
