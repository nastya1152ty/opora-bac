import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateEventInput {
  @Field()
  name: string;

  @Field()
  description: string;

  @Field()
  dateStart: Date;

  @Field()
  dateEnd: Date;

  @Field(() => [Number])
  citizenIds: [number];
}
