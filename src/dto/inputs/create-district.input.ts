import { InputType, Field } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';
import { IsNotEmpty, IsString } from 'class-validator';

@InputType()
export class CreateDistrictInput implements Prisma.DistrictCreateInput {
  @Field()
  @IsString()
  @IsNotEmpty()
  name: string;
}
