import { InputType, PartialType } from '@nestjs/graphql';
import { CreateCuratorInput } from './create-curator.input';

@InputType()
export class UpdateCuratorInput extends PartialType(CreateCuratorInput) {}
