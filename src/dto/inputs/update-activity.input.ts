import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class UpdateActivityInput {
  @Field({ nullable: true })
  end_date: Date;

  @Field(() => [String],{ nullable: true })
  comment: [string];

  @Field({ nullable: true })
  support_measureId: number;
}
