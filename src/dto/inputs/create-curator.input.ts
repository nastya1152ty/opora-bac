import { InputType, Field } from '@nestjs/graphql';
import {
  IsEmail,
  IsMobilePhone,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

@InputType()
export class CreateCuratorInput {
  @Field()
  @IsString()
  @IsNotEmpty()
  first_name: string;

  @Field()
  @IsString()
  @IsNotEmpty()
  last_name: string;

  @Field({ nullable: true })
  @IsString()
  @IsOptional()
  second_name: string;

  @Field()
  @IsEmail()
  email: string;

  @Field()
  @IsString()
  password: string;

  @Field()
  @IsMobilePhone('ru-RU')
  phone: string;

  @Field()
  @IsNotEmpty()
  districtId: number;

  @Field()
  @IsNotEmpty()
  role: 'ADMIN' | 'CURATOR';
}
