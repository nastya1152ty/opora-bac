import { InputType, Field } from '@nestjs/graphql';
import { Prisma } from '@prisma/client';
import { IsEmail, IsMobilePhone } from 'class-validator';

@InputType()
export class UpdateTaskInput {
    @Field({ nullable: true })
    description: string;
  
    @Field({ nullable: true })
    dateStart: Date;
  
    @Field({ nullable: true })
    dateEnd: Date;

    @Field({ nullable: true })
    activityId: number;
}