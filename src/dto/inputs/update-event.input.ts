import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class UpdateEventInput {
  @Field({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  description?: string;

  @Field({ nullable: true })
  dateStart?: Date;

  @Field({ nullable: true })
  dateEnd?: Date;

  @Field(() => [Number], { nullable: true })
  citizenIds: [number];
}
