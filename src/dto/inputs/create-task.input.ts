import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateTaskInput {
  @Field()
  description: string;

  @Field()
  dateStart: Date;

  @Field()
  dateEnd: Date;

  @Field({ nullable: true })
  activityId: number;
}
