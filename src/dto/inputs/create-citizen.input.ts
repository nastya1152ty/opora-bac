import { InputType, Field } from '@nestjs/graphql';
import {
  IsEmail,
  IsMobilePhone,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

@InputType()
export class CreateCitizenInput {
  @Field()
  @IsString()
  @IsNotEmpty()
  first_name: string;

  @Field()
  @IsString()
  @IsNotEmpty()
  last_name: string;

  @Field({ nullable: true })
  @IsString()
  @IsOptional()
  second_name: string;

  @Field({ nullable: true })
  @IsString()
  @IsOptional()
  organizationName: string;

  @Field()
  @IsEmail()
  email: string;

  @Field()
  @IsMobilePhone('ru-RU')
  phone: string;

  @Field({ nullable: true })
  user_bot_id: string;

  @Field()
  inn: string;

  @Field({defaultValue: true})
  isApproved: boolean;

  @Field()
  @IsNotEmpty()
  curatorId: string;

  @Field()
  @IsNotEmpty()
  category: 'INDIVIDUAL' | 'ORGANIZATION' | 'IP';
}
