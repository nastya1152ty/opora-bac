import { Field, ArgsType, InputType } from '@nestjs/graphql';

@ArgsType()
@InputType()
export class GetDistrictArgs {
  @Field({ nullable: true })
  name?: string;
}
