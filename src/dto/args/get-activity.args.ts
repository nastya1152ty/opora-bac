import { Field, ArgsType, InputType } from '@nestjs/graphql';

@ArgsType()
@InputType()
export class GetActivityArgs {
  @Field({ nullable: true })
  citizenId?: number;

  @Field({ nullable: true })
  curatorId?: string;

  @Field({ nullable: true })
  lastThirty?: boolean;

  @Field({ nullable: true })
  user_bot_id?: string;
}
