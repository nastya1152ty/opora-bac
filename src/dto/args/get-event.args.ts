import { Field, ArgsType, InputType } from '@nestjs/graphql';

@ArgsType()
@InputType()
export class GetEventArgs {
  @Field({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  curatorId: string;

  @Field({ nullable: true })
  dateEnd?: boolean;

  @Field({ nullable: true })
  citizenId: number;
}
