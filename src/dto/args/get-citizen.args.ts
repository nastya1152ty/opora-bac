import { Field, ArgsType, InputType } from '@nestjs/graphql';

@ArgsType()
@InputType()
export class GetCitizenArgs {
  @Field({ nullable: true })
  id?: number;

  @Field({nullable: true})
  botId?: string;
}
