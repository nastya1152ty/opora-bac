import { Field, ArgsType, InputType } from '@nestjs/graphql';

@ArgsType()
@InputType()
export class GetCuratorArgs {
  @Field({ nullable: true })
  id?: string;

  @Field({ nullable: true })
  first_name?: string;

  @Field({ nullable: true })
  last_name?: string;

  @Field({ nullable: true })
  second_name?: string;

  @Field({ nullable: true })
  email?: string;

  @Field({ nullable: true })
  phone?: string;

  @Field({ nullable: true })
  role?: 'ADMIN' | 'CURATOR';
}
