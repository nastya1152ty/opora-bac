import { Field, ArgsType, InputType } from '@nestjs/graphql';

@ArgsType()
@InputType()
export class GetCitizensArgs {
  @Field({ nullable: true })
  curatorId?: string;

  @Field({ nullable: true })
  first_name?: string;

  @Field({ nullable: true })
  last_name?: string;

  @Field({ nullable: true })
  second_name?: string;

  @Field({ nullable: true })
  organizationName?: string;

  @Field({ nullable: true })
  isApproved?: boolean;

  @Field({ nullable: true })
  phone?: string;

  @Field({ nullable: true })
  inn?: string;

  @Field({ nullable: true })
  category?: 'IP' | 'INDIVIDUAL' | 'ORGANIZATION';
}
