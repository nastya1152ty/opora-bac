import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import {
  ApolloFederationDriver,
  ApolloFederationDriverConfig,
} from '@nestjs/apollo';
import { PrismaModule } from './database/prisma.module';
import { ConfigModule } from '@nestjs/config';
import { GraphQLError, GraphQLFormattedError } from 'graphql';
import { AuthModule } from './auth/auth.module';
import { DistrictService } from './services/district.service';
import { DistrictResolver } from './resolvers/district.resolver';
import { CuratorResolver } from './resolvers/curator.resolver';
import { CuratorService } from './services/curator.service';
import { CitizenResolver } from './resolvers/citizen.resolver';
import { CitizenService } from './services/citizen.service';
import { EventResolver } from './resolvers/event.resolver';
import { EventService } from './services/event.service';
import { SupportMeasuresResolver } from './resolvers/support_measures.resolver';
import { SupportMeasuresService } from './services/support_measures.service';
import { TaskResolver } from './resolvers/task.resolver';
import { TaskService } from './services/task.service';
import { ActivityResolver } from './resolvers/activity.resolver';
import { ActivityService } from './services/activity.service';
import { AuthService } from './auth/auth.service';
import { ExcelService } from './services/excel.service';
import { ExcelController } from './resolvers/excel.resolver';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    GraphQLModule.forRoot<ApolloFederationDriverConfig>({
      driver: ApolloFederationDriver,
      autoSchemaFile: {
        federation: 2,
      },
      playground: true, 
      path: '/graphql',
      introspection: true, 
      formatError: (error: GraphQLError) => {
        const formattedError: GraphQLFormattedError = {
          message: error.message,
          extensions: {
            ...error.extensions,
          },
        };
        delete formattedError.extensions.stacktrace;
        return formattedError;
      },
    }),
    PrismaModule,
    AuthModule,
  ],
  providers: [
    DistrictService,
    DistrictResolver,
    CuratorResolver,
    CuratorService,
    CitizenResolver,
    CitizenService,
    EventResolver,
    EventService,
    SupportMeasuresResolver,
    SupportMeasuresService,
    TaskResolver,
    TaskService,
    ActivityResolver,
    ActivityService,
    AuthService,
    ExcelService
  ],
  controllers: [ExcelController],
})
export class AppModule {}
