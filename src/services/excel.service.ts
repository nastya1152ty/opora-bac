import { Injectable, NotFoundException, Res } from '@nestjs/common';
import { PrismaService } from '../database/prisma.service';
import * as ExcelJS from 'exceljs';
import { CuratorService } from './curator.service';
import { Response } from 'express';
import { ActivityService } from './activity.service';

@Injectable()
export class ExcelService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly curatorService: CuratorService,
    private readonly activityService: ActivityService
  ) {}

  async downloadExcel(@Res() res: Response) {
    const columns = ['Куратор', 'Район', 'Количество_клиентов', 'Рейтинг'];
    const curators = await this.curatorService.getAllCurators();
    console.log(curators)
    if (!curators.length) {
      throw new NotFoundException('No data to download');
    }
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Curator');

    worksheet.columns = columns.map((column) => {
      return {
        header: column,
        key: column.toLowerCase(),
        width: 15,
        style: { font: { bold: true } },
      };
    });

    for (const curator of curators) {
      let rating = await this.activityService.rating(curator.id);
      if (!rating) {
        rating = 0;
      }
      const curatorData = {
        куратор: `${curator.first_name} ${curator.last_name}`,
        район: curator.district ? curator.district.name : 'Не указан',
        количество_клиентов: curator._count.citizen.toString(),
        рейтинг: rating.toString(),
      };
      worksheet.addRow(curatorData);
    }

    res.setHeader(
      'Content-Type',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    );
    res.setHeader('Content-Disposition', 'attachment; filename=Citizens.xlsx');

    await workbook.xlsx.write(res);
  }
}
