import { BadRequestException, Injectable } from '@nestjs/common';
import { PrismaService } from '../database/prisma.service';
import { CitizenService } from './citizen.service';
import { UpdateActivityInput } from 'src/dto/inputs/update-activity.input';
import { GetActivityArgs } from 'src/dto/args/get-activity.args';
import { Prisma } from '@prisma/client';

@Injectable()
export class ActivityService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly citizen: CitizenService
  ) {}

  async create(user_bot_id: string) {
    const citizen = await this.citizen.getOne(undefined, user_bot_id);
    const activity = await this.prisma.activity.create({
      data: {
        curatorId: citizen.curatorId,
        request_date: new Date(),
        citizenId: citizen.id,
      },
    });
    return activity;
  }

  async confirmActivity(id: number) {
    const activity = await this.prisma.activity.update({
      where: { id },
      data: { confirm_date: new Date() },
    });
    return activity;
  }

  async update(id: number, input: UpdateActivityInput) {
    const activity = (await this.getOne(id)).comment;
    let updatedComments = activity;
    if (input.comment) {
      updatedComments = [...activity, ...input.comment];
    }
    const updatedData = {
      ...input,
      comment: updatedComments,
    };
    const updateActivity = this.prisma.activity.update({
      where: { id },
      data: updatedData,
    });
    return updateActivity;
  }

  async reaction(id: number, reaction: number) {
    const activity = this.prisma.activity.update({
      where: { id },
      data: { reaction },
    });
    return activity;
  }

  async rating(curatorId: string): Promise<Number> {
    const activities = await this.prisma.activity.findMany({
      where: {
        curatorId,
        reaction: {
          not: null,
        },
      },
    });
    const { totalRating, count } = activities.reduce(
      (acc, activity) => {
        acc.totalRating += activity.reaction;
        acc.count++;
        return acc;
      },
      { totalRating: 0, count: 0 }
    );
    const averageRating = Math.round(totalRating / count);
    const endActivity = await this.getConfirmActivitys(curatorId);
    const rating = Math.round(0.6 * averageRating + 0.4 * endActivity);

    return rating;
  }

  async getConfirmActivitys(curatorId: string): Promise<number> {
    const activities = await this.prisma.activity.count({
      where: {
        curatorId,
        end_date: {
          not: null,
        },
      },
    });
    const getCuratorsActivity = await this.getCuratorsActivity();
    const rating =
      (activities - getCuratorsActivity.min.count) /
      (getCuratorsActivity.max.count - getCuratorsActivity.min.count);
    return rating;
  }
  async getCuratorsActivity() {
    try {
      const curators = await this.prisma.curator.findMany({
        where: {
          role: 'CURATOR',
        },
      });
      const curatorActivities = await Promise.all(
        curators.map(async (curator) => {
          const activities = await this.prisma.activity.findMany({
            where: {
              curatorId: curator.id,
              end_date: {
                not: null,
              },
            },
          });
          return {
            curatorId: curator.id,
            count: activities.length,
          };
        })
      );
      if (curatorActivities.length === 0) {
        return {
          max: null,
          min: null,
        };
      }
      let max = curatorActivities[0];
      let min = curatorActivities[0];

      for (const activity of curatorActivities) {
        if (activity.count > max.count) {
          max = activity;
        }
        if (activity.count < min.count) {
          min = activity;
        }
      }
      return {
        max,
        min,
      };
    } catch (error) {
      throw new BadRequestException();
    }
  }

  async getOne(id: number) {
    const activity = this.prisma.activity.findUnique({
      where: { id },
    });
    return activity;
  }

  async getMany(getActivityArgs?: GetActivityArgs) {
    let { curatorId, citizenId, lastThirty, user_bot_id } = getActivityArgs;
    let citizen;
    if (user_bot_id) {
      citizen = await this.citizen.getOne(undefined, user_bot_id);
      citizenId = citizen.id;
    }

    let query: Prisma.ActivityFindManyArgs = {
      where: {
        ...(curatorId && { curatorId }),
        ...(citizenId && { citizenId }),
      },
    };

    if (lastThirty) {
      const thirtyDaysAgo = new Date();
      thirtyDaysAgo.setDate(thirtyDaysAgo.getDate() - 30);

      query.where = {
        ...query.where,
        request_date: {
          gt: thirtyDaysAgo,
        },
      };
    }

    const activity = await this.prisma.activity.findMany({
      ...query,
    });

    return activity;
  }

  async delete(id: number) {
    return await this.prisma.activity.delete({
      where: { id },
    });
  }
}
