import { Injectable } from '@nestjs/common';
import { PrismaService } from '../database/prisma.service';
import { CreateSupportMeasuresInput } from 'src/dto/inputs/create-support_measures.input';

@Injectable()
export class SupportMeasuresService {
  constructor(private readonly prisma: PrismaService) {}

  async findOne(id: number) {
    return await this.prisma.support_measures.findFirst({
      where: { id },
    });
  }

  async findMany() {
    return await this.prisma.support_measures.findMany();
  }

  async create(createSupportMeasuresInput: CreateSupportMeasuresInput) {
    return await this.prisma.support_measures.create({
      data: createSupportMeasuresInput,
    });
  }
}
