import { Injectable } from '@nestjs/common';
import { PrismaService } from '../database/prisma.service';
import { CreateDistrictInput } from 'src/dto/inputs/create-district.input';

@Injectable()
export class DistrictService {
  constructor(private readonly prisma: PrismaService) {}

  async findOne(id: number) {
    return await this.prisma.district.findFirst({
      where: { id },
    });
  }

  async create(createDistrictInput: CreateDistrictInput) {
    return await this.prisma.district.create({ data: createDistrictInput });
  }

  async getMany(name?: string) {
    return await this.prisma.district.findMany({
      where: {
        name: {
          startsWith: name,
        },
      },
      include: { curator: true },
    });
  }
}
