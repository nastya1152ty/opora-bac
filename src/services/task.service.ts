import { Injectable } from '@nestjs/common';
import { PrismaService } from '../database/prisma.service';
import { CreateTaskInput } from 'src/dto/inputs/create-task.input';
import { UpdateTaskInput } from 'src/dto/inputs/update-task.input';

@Injectable()
export class TaskService {
  constructor(private readonly prisma: PrismaService) {}

  async create(id: string, input: CreateTaskInput) {
    const task = await this.prisma.task.create({
      data: { ...input, curatorId: id },
    });
    return task;
  }

  async update(id: number, input: UpdateTaskInput) {
    const task = this.prisma.task.update({
      where: { id },
      data: input,
    });
    return task;
  }

  async getOne(id: number) {
    const task = this.prisma.task.findUnique({
      where: { id },
    });
    return task;
  }

  async getMany(curatorId: string | null) {
    const tasks = await this.prisma.task.findMany({
      where: {
        ...(curatorId && { curatorId }),
      },
    });
    return tasks;
  }

  async delete(id: number) {
    return await this.prisma.task.delete({
      where: { id },
    });
  }
}
