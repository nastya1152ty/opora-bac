import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from '../database/prisma.service';
import { CreateCitizenInput } from 'src/dto/inputs/create-citizen.input';
import { UpdateCitizenInput } from 'src/dto/inputs/update-citizen.input';
import { GetCitizensArgs } from 'src/dto/args/get-citizens.args';

@Injectable()
export class CitizenService {
  constructor(private readonly prisma: PrismaService) {}

  async create(createCitizenInput: CreateCitizenInput) {
    const citizen = await this.prisma.citizen.create({
      data: { ...createCitizenInput, code: Date.now().toString(10).slice(-5) },
    });
    citizen.code = citizen.id.toString() + citizen.code.toString().slice(0, 6);
    return await this.prisma.citizen.update({
      where: { id: citizen.id },
      data: { ...citizen },
    });
  }

  async getOne(id?: number, user_bot_id?: string) {
    const citizen = await this.prisma.citizen.findFirst({
      where: {
        ...(id && { id }),
        ...(user_bot_id && { user_bot_id }),
      },
    });
    if (!citizen) {
      new NotFoundException(`Citizen with ${id} not found`);
    }
    return citizen;
  }

  async getMany(curatorId?: string) {
    return await this.prisma.citizen.findMany({
      where: {
        ...(curatorId && { curatorId }),
      },
    });
  }

  async getCitizenByArgs(getCitizensArgs: GetCitizensArgs) {
    const {
      first_name,
      last_name,
      second_name,
      category,
      phone,
      curatorId,
      organizationName,
      isApproved,
      inn,
    } = getCitizensArgs;

    return this.prisma.citizen.findMany({
      where: {
        first_name: first_name,
        last_name: last_name,
        second_name: second_name,
        category: category,
        phone: phone,
        curatorId,
        organizationName,
        isApproved,
        inn,
      },
    });
  }

  async update(id: number, updateCitizenInput: UpdateCitizenInput) {
    return this.prisma.citizen.update({
      where: { id },
      data: { ...updateCitizenInput },
    });
  }

  async chatBotAuth(code: string, user_bot_id: string) {
    const citizen = await this.prisma.citizen.findFirst({
      where: { code, isApproved: true },
    });
    if (!citizen) {
      throw new NotFoundException(`User with code ${code} not found`);
    }
    citizen.user_bot_id = user_bot_id;
    return await this.update(citizen.id, citizen);
  }

  async softDeleteCitizen(id: number) {
    return this.prisma.citizen.update({
      where: {
        id,
      },
      data: { deletedAt: new Date() },
    });
  }
}
