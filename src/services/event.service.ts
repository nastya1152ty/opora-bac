import { BadRequestException, Injectable } from '@nestjs/common';
import { PrismaService } from '../database/prisma.service';
import { CreateEventInput } from 'src/dto/inputs/create-event.input';
import { UpdateEventInput } from 'src/dto/inputs/update-event.input';
import { GetEventArgs } from 'src/dto/args/get-event.args';
import { CitizenService } from './citizen.service';

@Injectable()
export class EventService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly citizen: CitizenService
  ) {}

  async create(id: string, input: CreateEventInput) {
    const event = await this.prisma.event.create({
      data: {
        name: input.name,
        description: input.description,
        dateStart: input.dateStart,
        dateEnd: input.dateEnd,
        curatorId: id,
      },
    });
    Promise.all(
      input.citizenIds.map((citizenId) => {
        return this.prisma.citizenOnEvent.create({
          data: {
            citizenId,
            eventId: event.id,
          },
        });
      })
    );
    return event;
  }

  async findOne(id: number) {
    return await this.prisma.event.findFirst({
      where: { id },
    });
  }

  async findEventWithCitizen(id: number) {
    return await this.prisma.citizenOnEvent.findMany({
      where: {
        eventId: id,
      },
    });
  }

  async getMany(getEventArgs?: GetEventArgs) {
    const currentDate = new Date();
    let dateEndCondition = {};

    if (getEventArgs.dateEnd === true) {
      dateEndCondition = {};
    } else if (getEventArgs.dateEnd === false) {
      dateEndCondition = { gt: currentDate };
    } else {
      dateEndCondition = { gt: currentDate };
    }
    return await this.prisma.event.findMany({
      where: {
        name: {
          startsWith: getEventArgs.name,
        },
        curatorId: getEventArgs.curatorId,
        dateEnd: dateEndCondition,
        citizenOnEvent: {
          some: {
            citizenId: getEventArgs.citizenId,
          },
        },
      },
      include: { curator: true },
    });
  }

  //bot
  async getEventsWithCitizenStatus(user_bot_id: string) {
    const citizenId = (await this.citizen.getOne(undefined, user_bot_id)).id;
    const thirtyDaysAgo = new Date();
    thirtyDaysAgo.setDate(thirtyDaysAgo.getDate() - 30);

    const events = await this.prisma.event.findMany({
      where: {
        dateEnd: {
          gt: thirtyDaysAgo,
        },
        citizenOnEvent: {
          some: {
            citizenId: citizenId,
            OR: [{ isAttending: true }, { isAttending: null }],
          },
        },
      },
    });

    return events;
  }
  //bot
  async isAttending(
    user_bot_id: string,
    eventId: number,
    isAttending: boolean
  ) {
    const citizenId = (await this.citizen.getOne(undefined, user_bot_id)).id;
    const event = await this.prisma.citizenOnEvent.update({
      where: { eventId_citizenId: { eventId, citizenId } },
      data: {
        isAttending: isAttending,
      },
    });
    return event;
  }

  async update(id: number, input: UpdateEventInput) {
    const event = await this.prisma.event.update({
      where: { id },
      data: {
        name: input.name,
        description: input.description,
        dateStart: input.dateStart,
        dateEnd: input.dateEnd,
      },
    });
    if (input.citizenIds) {
      await Promise.all(
        input.citizenIds.map(async (citizenId) => {
          const existingRecord = await this.prisma.citizenOnEvent.findUnique({
            where: {
              eventId_citizenId: {
                citizenId,
                eventId: event.id,
              },
            },
          });

          if (!existingRecord) {
            await this.prisma.citizenOnEvent.create({
              data: {
                citizenId,
                eventId: event.id,
              },
            });
          }
        })
      );
    }
    return event;
  }

  async deleteEventOnCitizen(eventId: number, citizenId: number) {
    await this.prisma.citizenOnEvent.delete({
      where: {
        eventId_citizenId: {
          eventId,
          citizenId,
        },
      },
    });
  }
}
