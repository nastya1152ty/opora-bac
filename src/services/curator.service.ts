import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from '../database/prisma.service';
import { CreateCuratorInput } from 'src/dto/inputs/create-curator.input';
import { GetCuratorArgs } from 'src/dto/args/get-curator.args';
import { encodePassword } from 'src/utils/encrypt';
import { UpdateCuratorInput } from 'src/dto/inputs/update-curator.input';

@Injectable()
export class CuratorService {
  constructor(private readonly prisma: PrismaService) {}

  async createCurator(createCuratorInput: CreateCuratorInput) {
    const district = await this.prisma.district.findUnique({
      where: { id: createCuratorInput.districtId },
    });
    if (!district)
      throw new BadRequestException(
        `Invalid district ${createCuratorInput.districtId}`
      );
    const curator = await this.getCuratorByArgs(createCuratorInput);
    if (curator) {
      throw new BadRequestException('Already exist!');
    }
    const password = await encodePassword(createCuratorInput.password);
    return await this.prisma.curator.create({
      data: { ...createCuratorInput, password },
    });
  }

  async getCurator(id: string) {
    const curator = await this.prisma.curator.findUnique({
      where: { id },
    });
    if (!curator) {
      new NotFoundException(`Account with ${id} not found`);
    }
    return curator;
  }

  async getcurators() {
    return this.prisma.curator.findMany();
  }

  async getAllCurators() {
    return this.prisma.curator.findMany({
      where: {
        role: 'CURATOR',
      },
      select: {
        id: true,
        first_name: true,
        last_name: true,
        district: true,
        _count: {
          select: {
            citizen: true,
          },
        },
      },
    });
  }
  async countCitizen() {
    return this.prisma.curator.findMany();
  }

  async getCuratorsByArgs(getCuratorArgs: GetCuratorArgs) {
    const { id, first_name, last_name, second_name, email, role, phone } =
      getCuratorArgs;

    return this.prisma.curator.findMany({
      where: {
        id: id,
        first_name: first_name,
        last_name: last_name,
        second_name: second_name,
        email: email,
        role: role,
        phone: phone,
      },
    });
  }

  async getCuratorByArgs(getCuratorArgs: GetCuratorArgs) {
    const { id, first_name, last_name, second_name, email, role, phone } =
      getCuratorArgs;

    return this.prisma.curator.findFirst({
      where: {
        id: id,
        first_name: first_name,
        last_name: last_name,
        second_name: second_name,
        email: email,
        role: role,
        phone: phone,
      },
    });
  }

  async softDelete(id: string) {
    const curator = await this.getCurator(id);

    return this.prisma.curator.update({
      where: {
        role_id: { id: id, role: curator.role },
      },
      data: { deletedAt: new Date() },
    });
  }

  async updateCurator(id: string, updateCuratorInput: UpdateCuratorInput) {
    try {
      if (updateCuratorInput.password) {
        updateCuratorInput.password = await encodePassword(
          updateCuratorInput.password
        );
      }
      return await this.prisma.curator.update({
        where: { id },
        data: { ...updateCuratorInput },
      });
    } catch (e) {
      throw new BadRequestException();
    }
  }
}
