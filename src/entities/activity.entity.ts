import {
  ObjectType,
  Field,
  ID,
  Directive,
  GraphQLISODateTime,
} from '@nestjs/graphql';
import { Activity as ActivityDB } from '@prisma/client';

@ObjectType()
@Directive('@key(fields: "id")')
export class Activity {
  @Field(() => ID)
  id: ActivityDB[`id`];

  @Field(() => [String])
  comment: ActivityDB[`comment`];

  @Field((type) => GraphQLISODateTime)
  request_date: ActivityDB['request_date'];

  @Field((type) => GraphQLISODateTime, { nullable: true })
  confirm_date: ActivityDB[`confirm_date`];

  @Field((type) => GraphQLISODateTime, { nullable: true })
  end_date: ActivityDB[`end_date`];

  @Field(() => String)
  curatorId: ActivityDB[`curatorId`];

  @Field(() => Number, { nullable: true })
  reaction: ActivityDB[`reaction`];

  @Field(() => Number)
  citizenId: ActivityDB[`citizenId`];

  @Field(() => Number, { nullable: true })
  support_measureId: ActivityDB[`support_measureId`];
}
