import { ObjectType, Field, Int, ID, Directive } from '@nestjs/graphql';
import { Curator as CuratorDB } from '@prisma/client';

@ObjectType()
@Directive('@key(fields: "id")')
export class Curator {
  @Field(() => ID)
  id: CuratorDB[`id`];

  @Field(() => String)
  first_name: CuratorDB[`first_name`];

  @Field(() => String)
  last_name: CuratorDB[`last_name`];

  @Field(() => String, { nullable: true })
  second_name: CuratorDB[`second_name`];

  @Field(() => String)
  email: CuratorDB[`email`];

  @Field(() => String)
  password: CuratorDB['password'];

  @Field(() => String)
  role: CuratorDB[`role`];

  @Field(() => String)
  phone: CuratorDB[`phone`];

  @Field(() => Date, { nullable: true })
  deletedAt: Date;

  @Field(() => Int)
  districtId: CuratorDB[`districtId`];
}
