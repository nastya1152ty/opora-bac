import {
  ObjectType,
  Field,
  ID,
  Directive,
  GraphQLISODateTime,
} from '@nestjs/graphql';
import { Task as TaskDB } from '@prisma/client';

@ObjectType()
@Directive('@key(fields: "id")')
export class Task {
  @Field(() => ID)
  id: TaskDB[`id`];

  @Field(() => String)
  description: TaskDB[`description`];

  @Field((type) => GraphQLISODateTime)
  dateStart: TaskDB['dateStart'];

  @Field((type) => GraphQLISODateTime)
  dateEnd: TaskDB[`dateEnd`];

  @Field(() => String)
  curatorId: TaskDB[`curatorId`];

  @Field(() => Number, { nullable: true })
  activityId: TaskDB[`activityId`];
}
