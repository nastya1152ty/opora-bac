import { ObjectType, Field, Int, Directive } from '@nestjs/graphql';
import { CitizenOnEvent as CitizenOnEventDB } from '@prisma/client';

@ObjectType()
export class CitizenOnEvent {
  @Field(() => Int)
  citizenId: CitizenOnEventDB[`citizenId`];

  @Field(() => Int)
  eventId: CitizenOnEventDB[`eventId`];

  @Field(() => Boolean, {nullable: true})
  isAttending: CitizenOnEventDB[`isAttending`];
}
