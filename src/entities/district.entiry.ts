import { ObjectType, Field, Int, ID, Directive } from '@nestjs/graphql';
import { District as DistrictDB } from '@prisma/client';

@ObjectType()
@Directive('@key(fields: "id")')
export class District {
  @Field(() => ID)
  id: DistrictDB[`id`];

  @Field(() => String)
  name: DistrictDB[`name`];
}
