import {
  ObjectType,
  Field,
  ID,
  Directive,
  GraphQLISODateTime,
} from '@nestjs/graphql';
import { Event as EventDB } from '@prisma/client';

@ObjectType()
@Directive('@key(fields: "id")')
export class Event {
  @Field(() => ID)
  id: EventDB[`id`];

  @Field(() => String)
  name: EventDB[`name`];

  @Field(() => String)
  description: EventDB[`description`];

  @Field((type) => GraphQLISODateTime)
  dateStart: EventDB['dateStart'];

  @Field((type) => GraphQLISODateTime)
  dateEnd: EventDB[`dateEnd`];

  @Field(() => String)
  curatorId: EventDB[`curatorId`];
}
