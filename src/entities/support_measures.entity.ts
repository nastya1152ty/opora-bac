import { ObjectType, Field, ID, Directive } from '@nestjs/graphql';
import { Support_measures as Support_measuresDB } from '@prisma/client';

@ObjectType()
@Directive('@key(fields: "id")')
export class SupportMeasures {
  @Field(() => ID)
  id: Support_measuresDB[`id`];

  @Field(() => String)
  value: Support_measuresDB[`value`];
}
