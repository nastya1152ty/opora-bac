import { ObjectType, Field, ID, Directive } from '@nestjs/graphql';
import { Citizen as CitizenDB } from '@prisma/client';

@ObjectType()
@Directive('@key(fields: "id")')
export class Citizen {
  @Field(() => ID)
  id: CitizenDB[`id`];

  @Field(() => String)
  first_name: CitizenDB[`first_name`];

  @Field(() => String)
  last_name: CitizenDB[`last_name`];

  @Field(() => String, { nullable: true })
  second_name: CitizenDB[`second_name`];

  @Field(() => String, { nullable: true })
  organizationName: CitizenDB[`organizationName`];

  @Field(() => String, { nullable: true })
  email: CitizenDB[`email`];

  @Field(() => String)
  inn: CitizenDB[`inn`];

  @Field(() => String)
  code: CitizenDB[`code`];

  @Field(() => Boolean, { defaultValue: true })
  isApproved: CitizenDB[`isApproved`];

  @Field(() => String, { nullable: true })
  user_bot_id: CitizenDB[`user_bot_id`];

  @Field(() => String)
  phone: CitizenDB[`phone`];

  @Field(() => String)
  curatorId: CitizenDB[`curatorId`];

  @Field(() => String)
  category: CitizenDB[`category`];

  @Field(() => Date, { nullable: true })
  deletedAt: Date;
}
