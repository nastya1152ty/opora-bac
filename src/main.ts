import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { RolesGuard } from './auth/guards/roles.guard';
import { ValidationPipe } from '@nestjs/common';
import { ContentTypeMiddleware } from './contentType.middleware';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.use(new ContentTypeMiddleware().use);
  app.enableCors({ origin: true });

  app.useGlobalGuards(
    new RolesGuard(
      new JwtService({
        global: true,
        secret: process.env.JWT_SECRET,
        signOptions: { expiresIn: '1h' },
      }),
      new Reflector()
    )
  );

  app.useGlobalPipes(new ValidationPipe());
  await app.listen(process.env.APPLICATION_PORT);
}
bootstrap();
