import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { SupportMeasures } from 'src/entities/support_measures.entity';
import { SupportMeasuresService } from 'src/services/support_measures.service';
import { CreateSupportMeasuresInput } from 'src/dto/inputs/create-support_measures.input';

@Resolver(() => SupportMeasures)
export class SupportMeasuresResolver {
  constructor(
    private readonly supportMeasuresService: SupportMeasuresService
  ) {}

  @Mutation(() => SupportMeasures)
  createSupportMeasures(
    @Args('input') createSupportMeasuresInput: CreateSupportMeasuresInput
  ) {
    return this.supportMeasuresService.create(createSupportMeasuresInput);
  }

  @Query(() => [SupportMeasures])
  getsSupportMeasures() {
    return this.supportMeasuresService.findMany();
  }

  @Query(() => SupportMeasures, {nullable: true})
  getSupportMeasures(@Args('id') id: number) {
    return this.supportMeasuresService.findOne(id);
  }
}
