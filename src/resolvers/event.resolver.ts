import {
  Resolver,
  Query,
  Mutation,
  Args,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { Event } from 'src/entities/event.entity';
import { CreateEventInput } from 'src/dto/inputs/create-event.input';
import { EventService } from 'src/services/event.service';
import { GetEventArgs } from 'src/dto/args/get-event.args';
import { UpdateEventInput } from 'src/dto/inputs/update-event.input';
import { CitizenOnEvent } from 'src/entities/citizen_on_event.entity';
import { User } from 'src/auth/decorators/user.decorator';
import { BadRequestException, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';

@Resolver(() => Event)
export class EventResolver {
  constructor(private readonly eventService: EventService) {}

  @UseGuards(JwtAuthGuard)
  @Mutation(() => Event)
  createEvent(
    @User() token: any,
    @Args('CreateEventInput') createEventInput: CreateEventInput
  ) {
    return this.eventService.create(token.id, createEventInput);
  }

  @Query(() => [Event])
  getEvents(@Args() GetEventArgs?: GetEventArgs) {
    return this.eventService.getMany(GetEventArgs);
  }

  @Query(() => Event)
  async getEvent(@Args('id') id: number) {
    return await this.eventService.findOne(id);
  }

  @Mutation(() => Event, { nullable: true })
  async updateEvent(
    @Args('id') id: number,
    @Args('update') update: UpdateEventInput
  ) {
    try {
      return await this.eventService.update(id, update);
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  @Mutation(() => CitizenOnEvent)
  async isAttending(
    @Args('user_bot_id') user_bot_id: string,
    @Args('eventId') eventId: number,
    @Args('isAttending') isAttending: boolean
  ) {
    return await this.eventService.isAttending(
      user_bot_id,
      eventId,
      isAttending
    );
  }

  @Query(() => [Event], { nullable: true })
  async getEventsWithCitizenStatus(@Args('user_bot_id') user_bot_id: string) {
    return await this.eventService.getEventsWithCitizenStatus(user_bot_id);
  }

  @ResolveField('citizenOnEvent', () => [CitizenOnEvent])
  citizenOnEventWith(@Parent() event: Event) {
    return this.eventService.findEventWithCitizen(event.id);
  }

  @ResolveField('deleteEventOnCitizen', () => Boolean, {
    nullable: true,
  })
  async deleteEventOnCitizen(
    @Parent() event: Event,
    @Args('citizenId') citizenId: number
  ) {
    try {
      await this.eventService.deleteEventOnCitizen(event.id, citizenId);
      return true;
    } catch (error) {
      return false;
    }
  }
}
