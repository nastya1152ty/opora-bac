import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { District } from 'src/entities/district.entiry';
import { DistrictService } from 'src/services/district.service';
import { CreateDistrictInput } from 'src/dto/inputs/create-district.input';
import { GetDistrictArgs } from 'src/dto/args/get-district.args';

@Resolver(() => District)
export class DistrictResolver {
  constructor(private readonly districtService: DistrictService) {}

  @Mutation(() => District)
  createDistrict(
    @Args('CreateDistrictInput') CreateDistrictInput: CreateDistrictInput
  ) {
    return this.districtService.create(CreateDistrictInput);
  }

  @Query(() => [District])
  getDistricts(@Args() GetDistrictArgs?: GetDistrictArgs) {
    return this.districtService.getMany(GetDistrictArgs.name);
  }

  @Query(() => District)
  getDistrict(@Args('id') id: number) {
    return this.districtService.findOne(id);
  }
}
