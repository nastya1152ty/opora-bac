import {
  Resolver,
  Query,
  Mutation,
  Args,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { DistrictService } from 'src/services/district.service';
import { CuratorService } from 'src/services/curator.service';
import { Citizen } from 'src/entities/sitizen.entity';
import { CitizenService } from 'src/services/citizen.service';
import { CreateCitizenInput } from 'src/dto/inputs/create-citizen.input';
import {
  BadRequestException,
  Param,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { UpdateCitizenInput } from 'src/dto/inputs/update-citizen.input';
import { GetCitizenArgs } from 'src/dto/args/get-citizen.args';
import { GetCitizensArgs } from 'src/dto/args/get-citizens.args';
import { Curator } from 'src/entities/curator.entity';

@Resolver(() => Citizen)
export class CitizenResolver {
  constructor(
    private readonly citizenService: CitizenService,
    private readonly curatorService: CuratorService,
    private readonly districtService: DistrictService
  ) {}

  @UsePipes(new ValidationPipe())
  @Mutation(() => Citizen, {
    description: 'input type type: INDIVIDUAL | ORGANIZATION | IP',
  })
  createCitizen(
    @Args('createCitizenInput') createCitizenInput: CreateCitizenInput
  ) {
    return this.citizenService.create(createCitizenInput);
  }

  @Query(() => [Citizen])
  getCitizens(@Args() getCitizensArgs: GetCitizensArgs) {
    return this.citizenService.getCitizenByArgs(getCitizensArgs);
  }

  @Query(() => Citizen)
  getCitizen(@Args() getCitizenArgs: GetCitizenArgs) {
    return this.citizenService.getOne(getCitizenArgs.id, getCitizenArgs.botId);
  }

  @Mutation(() => Citizen)
  async chatBotAuth(
    @Args('code') code: string,
    @Args('user_bot_id') user_bot_id: string
  ) {
    return await this.citizenService.chatBotAuth(code, user_bot_id);
  }

  @Mutation(() => Citizen)
  async softDeleteCitizen(@Param('id') id: number) {
    try {
      return await this.citizenService.softDeleteCitizen(id);
    } catch (e) {
      throw new BadRequestException(`${e + this.softDeleteCitizen.name}`);
    }
  }

  @Mutation(() => Citizen)
  async updateCitizen(
    @Args('id') id: number,
    @Args('update') update: UpdateCitizenInput
  ) {
    return await this.citizenService.update(id, update);
  }

  @ResolveField('curator', () => Curator)
  getCitizenByCurztor(@Parent() citizen: Citizen) {
    return this.curatorService.getCurator(citizen.curatorId);
  }
}
