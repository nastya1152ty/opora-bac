import {
  Resolver,
  Query,
  Mutation,
  Args,
  Parent,
  ResolveField,
} from '@nestjs/graphql';
import { Activity } from 'src/entities/activity.entity';
import { ActivityService } from 'src/services/activity.service';
import { UpdateActivityInput } from 'src/dto/inputs/update-activity.input';
import { GetActivityArgs } from 'src/dto/args/get-activity.args';
import { SupportMeasuresService } from 'src/services/support_measures.service';
import { SupportMeasures } from 'src/entities/support_measures.entity';
import { CitizenService } from 'src/services/citizen.service';
import { Citizen } from 'src/entities/sitizen.entity';
import { BadRequestException } from '@nestjs/common';

@Resolver(() => Activity)
export class ActivityResolver {
  constructor(
    private readonly activityService: ActivityService,
    private readonly supportMeasuresService: SupportMeasuresService,
    private readonly citizenService: CitizenService
  ) {}

  @Mutation(() => Activity)
  createActivity(@Args('user_bot_id') user_bot_id: string) {
    return this.activityService.create(user_bot_id);
  }

  @Query(() => [Activity], { nullable: true })
  getActivitys(@Args() getActivityArgs?: GetActivityArgs) {
    return this.activityService.getMany(getActivityArgs);
  }

  @Query(() => Activity)
  getActivity(@Args('id') id: number) {
    return this.activityService.getOne(id);
  }

  @Query(() => Number)
  getRating(@Args('id') id: string): Promise<Number> {
    try {
      return this.activityService.rating(id);
    } catch (error) {
      throw new BadRequestException();
    }
  }

  @Mutation(() => Activity)
  confirmActivity(@Args('id') id: number) {
    return this.activityService.confirmActivity(id);
  }

  @Mutation(() => Activity)
  createReaction(@Args('id') id: number, @Args('reaction') reaction: number) {
    return this.activityService.reaction(id, reaction);
  }

  @Mutation(() => Activity)
  updateActivity(
    @Args('id') id: number,
    @Args('updateActivity') updateActivity: UpdateActivityInput
  ) {
    return this.activityService.update(id, updateActivity);
  }

  @Mutation(() => Activity)
  deleteActivity(@Args('id') id: number) {
    return this.activityService.delete(id);
  }

  @ResolveField('supportMeasures', () => SupportMeasures, { nullable: true })
  getActivityBySupportMeasures(@Parent() activity: Activity) {
    if (!activity.support_measureId) {
      return null;
    }
    return this.supportMeasuresService.findOne(activity.support_measureId);
  }

  @ResolveField('citizen', () => Citizen)
  getActivityByCitizen(@Parent() activity: Activity) {
    return this.citizenService.getOne(activity.citizenId);
  }
}
