import { Controller, Get, Res } from '@nestjs/common';
import { Response } from 'express';
import { ExcelService } from 'src/services/excel.service';

@Controller('excel')
export class ExcelController {
  constructor(private excelService: ExcelService) {}

  @Get('/download')
  async downloadExcelWithCitizen(@Res() res: Response) {
    await this.excelService.downloadExcel(res);
    res.end();
  }
}
