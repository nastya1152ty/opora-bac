import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { Task } from 'src/entities/task.entity';
import { TaskService } from 'src/services/task.service';
import { CreateTaskInput } from 'src/dto/inputs/create-task.input';
import { UpdateTaskInput } from 'src/dto/inputs/update-task.input';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { User } from 'src/auth/decorators/user.decorator';

@Resolver(() => Task)
export class TaskResolver {
  constructor(private readonly taskService: TaskService) {}

  @UseGuards(JwtAuthGuard)
  @Mutation(() => Task)
  createTask(
    @User() token: any,
    @Args('CreateTaskInput') createTaskInput: CreateTaskInput
  ) {
    return this.taskService.create(token.id, createTaskInput);
  }

  @Query(() => [Task])
  getTasks(@User() token: any) {
    const curatorId = token ? token.id : null;
    return this.taskService.getMany(curatorId);
  }

  @Query(() => Task)
  getTask(@Args('id') id: number) {
    return this.taskService.getOne(id);
  }

  @Mutation(() => Task)
  updateTask(
    @Args('id') id: number,
    @Args('updateTask') updateTask: UpdateTaskInput
  ) {
    return this.taskService.update(id, updateTask);
  }

  @Mutation(() => Task)
  deleteTask(@Args('id') id: number) {
    return this.taskService.delete(id);
  }
}
