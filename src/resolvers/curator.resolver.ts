import {
  Resolver,
  Query,
  Mutation,
  Args,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { District } from 'src/entities/district.entiry';
import { DistrictService } from 'src/services/district.service';
import { CreateDistrictInput } from 'src/dto/inputs/create-district.input';
import { Curator } from 'src/entities/curator.entity';
import { CuratorService } from 'src/services/curator.service';
import { CreateCuratorInput } from 'src/dto/inputs/create-curator.input';
import {
  BadRequestException,
  NotFoundException,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { GetCuratorArgs } from 'src/dto/args/get-curator.args';
import { UpdateCuratorInput } from 'src/dto/inputs/update-curator.input';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { User } from 'src/auth/decorators/user.decorator';
import { CitizenService } from 'src/services/citizen.service';
import { Citizen } from 'src/entities/sitizen.entity';
import { ActivityService } from 'src/services/activity.service';
import { Activity } from 'src/entities/activity.entity';

@Resolver(() => Curator)
export class CuratorResolver {
  constructor(
    private readonly curatorService: CuratorService,
    private readonly districtService: DistrictService,
    private readonly citizenService: CitizenService,
    private readonly activityService: ActivityService
  ) {}

  @UsePipes(new ValidationPipe())
  @Mutation(() => Curator, { description: 'input type role: ADMIN | CURATOR' })
  createCurator(
    @Args('createCuratorInput') createCuratorInput: CreateCuratorInput
  ) {
    return this.curatorService.createCurator(createCuratorInput);
  }

  @Query(() => [Curator])
  getCurators() {
    return this.curatorService.getcurators();
  }

  @UseGuards(JwtAuthGuard)
  @Query(() => Curator)
  getCurator(@User() token: any) {
    return this.curatorService.getCurator(token.id);
  }

  @Query(() => Curator, { nullable: true })
  getCuratorArgs(@Args('input') input: GetCuratorArgs) {
    return this.curatorService.getCuratorByArgs(input);
  }

  @Mutation(() => Curator)
  async softDelete(@Args('id') id: string) {
    try {
      return await this.curatorService.softDelete(id);
    } catch (e) {
      throw new BadRequestException(`${e + this.softDelete.name}`);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Mutation(() => Curator)
  async updateCurator(
    @User() token: any,
    @Args('input') input: UpdateCuratorInput
  ) {
    try {
      return await this.curatorService.updateCurator(token.id, input);
    } catch (e) {
      throw new NotFoundException(e.message.originalError);
    }
  }

  @ResolveField('district', () => District)
  getCuratorByDistrictId(@Parent() curator: Curator) {
    return this.districtService.findOne(curator.districtId);
  }

  @ResolveField('createDistrict', () => District)
  createDistrict(
    @Parent() curator: Curator,
    @Args('createDistrictInput')
    createDistrictInput: CreateDistrictInput
  ) {
    return this.districtService.create(createDistrictInput);
  }

  @ResolveField('citizen', () => [Citizen])
  getCuratorByCitizen(@Parent() curator: Curator) {
    return this.citizenService.getMany(curator.id);
  }

  @ResolveField('rating', () => Number)
  async getRating(@Parent() curator: Curator) {
    try {
      const rating = await this.activityService.rating(curator.id);
      return !rating ? 0 : rating;
    } catch (error) {
      return 0;
    }
  }
}
