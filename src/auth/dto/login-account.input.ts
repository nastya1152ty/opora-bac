import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class LoginAccountInput {
  @Field()
  email: string;

  @Field()
  password: string;
}
