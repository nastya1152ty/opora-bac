import { Field, ObjectType } from '@nestjs/graphql';
import { Curator } from 'src/entities/curator.entity';

@ObjectType()
export class LoginResponse {
  @Field()
  access_token: string;

  @Field(() => Curator)
  curator: Curator;
}
