import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from 'src/database/prisma.service';
import { Curator } from 'src/entities/curator.entity';
import { comparePassword } from 'src/utils/bcrypt';
import { compareEncodedPassword } from 'src/utils/encrypt';
import { LoginResponse } from './dto/login-response';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly prisma: PrismaService
  ) {}

  async validateAccount(email: string, password: string): Promise<Curator> {
    if (!email || !password) {
      throw new BadRequestException();
    }
    const curator = await this.prisma.curator.findFirst({ where: { email } });
    if (!curator) {
      throw new UnauthorizedException();
    }
    const isCorrectPassword = await comparePassword(password, curator.password);
    if (!isCorrectPassword) {
      await compareEncodedPassword(password, curator.password);
    }
    return curator;
  }

  async login(curator: Curator): Promise<LoginResponse> {
    const payload = { ...curator };
    return {
      access_token: this.jwtService.sign(payload),
      curator: curator,
    };
  }
}
