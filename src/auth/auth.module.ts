import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthResolver } from './auth.resolver';
import { JwtModule } from '@nestjs/jwt';
import { PrismaModule } from 'src/database/prisma.module';
import { JwtStrategy } from './guards/jwt.strategy';
import { CuratorService } from 'src/services/curator.service';

@Module({
  imports: [
    JwtModule.register({
      global: true,
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '13h' },
    }),
    PrismaModule,
  ],
  providers: [AuthResolver, AuthService, JwtStrategy, CuratorService],
})
export class AuthModule {}
