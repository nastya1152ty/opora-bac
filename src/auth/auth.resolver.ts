import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { Curator } from 'src/entities/curator.entity';
import { LoginResponse } from './dto/login-response';
import { LoginAccountInput } from 'src/auth/dto/login-account.input';

@Resolver(() => Curator)
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Mutation(() => LoginResponse)
  async login(@Args('input') loginAccountInput: LoginAccountInput): Promise<LoginResponse> {
    const account = await this.authService.validateAccount(
      loginAccountInput.email,
      loginAccountInput.password
    );
    return await this.authService.login(account);
  }
}
