-- CreateEnum
CREATE TYPE "Category" AS ENUM ('IP', 'INDIVIDUAL', 'ORGANIZATION');

-- CreateTable
CREATE TABLE "Sitizen" (
    "id" SERIAL NOT NULL,
    "first_name" TEXT NOT NULL,
    "last_name" TEXT NOT NULL,
    "second_name" TEXT,
    "organizationName" TEXT,
    "email" TEXT NOT NULL,
    "phone" TEXT NOT NULL,
    "deletedAt" TIMESTAMP(3),
    "user_vk_id" INTEGER,
    "inn" TEXT NOT NULL,
    "code" TEXT NOT NULL,
    "isApproved" BOOLEAN NOT NULL DEFAULT false,
    "category" "Category" NOT NULL,

    CONSTRAINT "Sitizen_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Sitizen_email_key" ON "Sitizen"("email");
