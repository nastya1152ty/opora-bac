-- CreateEnum
CREATE TYPE "Roles" AS ENUM ('ADMIN', 'CURATOR');

-- CreateTable
CREATE TABLE "Curator" (
    "id" TEXT NOT NULL,
    "first_name" TEXT NOT NULL,
    "last_name" TEXT NOT NULL,
    "second_name" TEXT,
    "role" "Roles" NOT NULL,
    "email" TEXT,
    "password" TEXT,
    "phone" TEXT NOT NULL,
    "deletedAt" TIMESTAMP(3),

    CONSTRAINT "Curator_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Curator_email_key" ON "Curator"("email");

-- CreateIndex
CREATE UNIQUE INDEX "Curator_password_key" ON "Curator"("password");

-- CreateIndex
CREATE UNIQUE INDEX "Curator_role_id_key" ON "Curator"("role", "id");
