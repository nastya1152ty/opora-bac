/*
  Warnings:

  - Added the required column `districtId` to the `Curator` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Curator" ADD COLUMN     "districtId" INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE "Curator" ADD CONSTRAINT "Curator_districtId_fkey" FOREIGN KEY ("districtId") REFERENCES "District"("id") ON DELETE CASCADE ON UPDATE CASCADE;
