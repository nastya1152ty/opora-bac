/*
  Warnings:

  - The primary key for the `CitizenOnEvent` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `sitizenId` on the `CitizenOnEvent` table. All the data in the column will be lost.
  - You are about to drop the `Sitizen` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `citizenId` to the `CitizenOnEvent` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "CitizenOnEvent" DROP CONSTRAINT "CitizenOnEvent_sitizenId_fkey";

-- DropForeignKey
ALTER TABLE "Sitizen" DROP CONSTRAINT "Sitizen_curatorId_fkey";

-- AlterTable
ALTER TABLE "CitizenOnEvent" DROP CONSTRAINT "CitizenOnEvent_pkey",
DROP COLUMN "sitizenId",
ADD COLUMN     "citizenId" INTEGER NOT NULL,
ADD CONSTRAINT "CitizenOnEvent_pkey" PRIMARY KEY ("eventId", "citizenId");

-- DropTable
DROP TABLE "Sitizen";

-- CreateTable
CREATE TABLE "Citizen" (
    "id" SERIAL NOT NULL,
    "first_name" TEXT NOT NULL,
    "last_name" TEXT NOT NULL,
    "second_name" TEXT,
    "organizationName" TEXT,
    "email" TEXT NOT NULL,
    "phone" TEXT NOT NULL,
    "deletedAt" TIMESTAMP(3),
    "user_vk_id" INTEGER,
    "inn" TEXT NOT NULL,
    "code" TEXT NOT NULL,
    "isApproved" BOOLEAN NOT NULL DEFAULT false,
    "category" "Category" NOT NULL,
    "curatorId" TEXT NOT NULL,

    CONSTRAINT "Citizen_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Citizen_email_key" ON "Citizen"("email");

-- AddForeignKey
ALTER TABLE "Citizen" ADD CONSTRAINT "Citizen_curatorId_fkey" FOREIGN KEY ("curatorId") REFERENCES "Curator"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CitizenOnEvent" ADD CONSTRAINT "CitizenOnEvent_citizenId_fkey" FOREIGN KEY ("citizenId") REFERENCES "Citizen"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
