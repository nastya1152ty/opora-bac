/*
  Warnings:

  - The primary key for the `Event` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The `id` column on the `Event` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "Event" DROP CONSTRAINT "Event_pkey",
DROP COLUMN "id",
ADD COLUMN     "id" SERIAL NOT NULL,
ADD CONSTRAINT "Event_pkey" PRIMARY KEY ("id");

-- CreateTable
CREATE TABLE "CitizenOnEvent" (
    "sitizenId" INTEGER NOT NULL,
    "eventId" INTEGER NOT NULL,

    CONSTRAINT "CitizenOnEvent_pkey" PRIMARY KEY ("eventId","sitizenId")
);

-- AddForeignKey
ALTER TABLE "CitizenOnEvent" ADD CONSTRAINT "CitizenOnEvent_sitizenId_fkey" FOREIGN KEY ("sitizenId") REFERENCES "Sitizen"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CitizenOnEvent" ADD CONSTRAINT "CitizenOnEvent_eventId_fkey" FOREIGN KEY ("eventId") REFERENCES "Event"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
