/*
  Warnings:

  - Added the required column `curatorId` to the `Sitizen` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Sitizen" ADD COLUMN     "curatorId" TEXT NOT NULL;

-- AddForeignKey
ALTER TABLE "Sitizen" ADD CONSTRAINT "Sitizen_curatorId_fkey" FOREIGN KEY ("curatorId") REFERENCES "Curator"("id") ON DELETE CASCADE ON UPDATE CASCADE;
