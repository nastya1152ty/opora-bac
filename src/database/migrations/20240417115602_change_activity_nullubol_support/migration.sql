-- DropForeignKey
ALTER TABLE "Activity" DROP CONSTRAINT "Activity_support_measureId_fkey";

-- AlterTable
ALTER TABLE "Activity" ALTER COLUMN "support_measureId" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "Activity" ADD CONSTRAINT "Activity_support_measureId_fkey" FOREIGN KEY ("support_measureId") REFERENCES "Support_measures"("id") ON DELETE SET NULL ON UPDATE CASCADE;
