/*
  Warnings:

  - You are about to drop the column `dateStart` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `deletedAt` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `typeId` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the `Event_Type` table. If the table is not empty, all the data it contains will be lost.
  - Made the column `email` on table `Curator` required. This step will fail if there are existing NULL values in that column.
  - Made the column `password` on table `Curator` required. This step will fail if there are existing NULL values in that column.
  - Added the required column `curatorId` to the `Event` table without a default value. This is not possible if the table is not empty.
  - Added the required column `dataStart` to the `Event` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_typeId_fkey";

-- AlterTable
ALTER TABLE "Curator" ALTER COLUMN "email" SET NOT NULL,
ALTER COLUMN "password" SET NOT NULL;

-- AlterTable
ALTER TABLE "Event" DROP COLUMN "dateStart",
DROP COLUMN "deletedAt",
DROP COLUMN "typeId",
ADD COLUMN     "curatorId" TEXT NOT NULL,
ADD COLUMN     "dataEnd" TIMESTAMP(3),
ADD COLUMN     "dataStart" TIMESTAMP(3) NOT NULL;

-- DropTable
DROP TABLE "Event_Type";

-- CreateTable
CREATE TABLE "Support_measures" (
    "id" SERIAL NOT NULL,
    "value" TEXT NOT NULL,

    CONSTRAINT "Support_measures_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Task" (
    "id" SERIAL NOT NULL,
    "dataStart" TIMESTAMP(3) NOT NULL,
    "dataEnd" TIMESTAMP(3),
    "description" TEXT NOT NULL,
    "curatorId" TEXT NOT NULL,
    "activityId" INTEGER,

    CONSTRAINT "Task_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Activity" (
    "id" SERIAL NOT NULL,
    "request_date" TIMESTAMP(3) NOT NULL,
    "confirm_date" TIMESTAMP(3),
    "end_date" TIMESTAMP(3),
    "comment" TEXT,
    "reaction" INTEGER,
    "citizenId" INTEGER NOT NULL,
    "curatorId" TEXT NOT NULL,
    "support_measureId" INTEGER NOT NULL,

    CONSTRAINT "Activity_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_curatorId_fkey" FOREIGN KEY ("curatorId") REFERENCES "Curator"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Task" ADD CONSTRAINT "Task_curatorId_fkey" FOREIGN KEY ("curatorId") REFERENCES "Curator"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Task" ADD CONSTRAINT "Task_activityId_fkey" FOREIGN KEY ("activityId") REFERENCES "Activity"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Activity" ADD CONSTRAINT "Activity_citizenId_fkey" FOREIGN KEY ("citizenId") REFERENCES "Citizen"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Activity" ADD CONSTRAINT "Activity_curatorId_fkey" FOREIGN KEY ("curatorId") REFERENCES "Curator"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Activity" ADD CONSTRAINT "Activity_support_measureId_fkey" FOREIGN KEY ("support_measureId") REFERENCES "Support_measures"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
