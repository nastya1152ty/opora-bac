/*
  Warnings:

  - The `comment` column on the `Activity` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "Activity" DROP COLUMN "comment",
ADD COLUMN     "comment" TEXT[];
