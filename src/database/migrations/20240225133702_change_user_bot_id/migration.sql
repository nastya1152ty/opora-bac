/*
  Warnings:

  - You are about to drop the column `user_vk_id` on the `Citizen` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Citizen" DROP COLUMN "user_vk_id",
ADD COLUMN     "user_bot_id" TEXT;
