-- CreateEnum
CREATE TYPE "EventType" AS ENUM ('MEETING_CURATOR', 'INFORMATION_FOR_YOU', 'SUPPORT_MEASURES');

-- CreateTable
CREATE TABLE "Event" (
    "id" TEXT NOT NULL,
    "type" "EventType" NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "dateStart" TIMESTAMP(3) NOT NULL,
    "deletedAt" TIMESTAMP(3),

    CONSTRAINT "Event_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Event_id_type_key" ON "Event"("id", "type");
