/*
  Warnings:

  - Made the column `dateEnd` on table `Event` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "CitizenOnEvent" ADD COLUMN     "isAttending" BOOLEAN;

-- AlterTable
ALTER TABLE "Event" ALTER COLUMN "dateEnd" SET NOT NULL;
