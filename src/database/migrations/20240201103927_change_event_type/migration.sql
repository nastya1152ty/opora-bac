/*
  Warnings:

  - You are about to drop the column `type` on the `Event` table. All the data in the column will be lost.

*/
-- DropIndex
DROP INDEX "Event_id_type_key";

-- AlterTable
ALTER TABLE "Event" DROP COLUMN "type",
ADD COLUMN     "typeId" INTEGER;

-- DropEnum
DROP TYPE "EventType";

-- CreateTable
CREATE TABLE "Event_Type" (
    "id" SERIAL NOT NULL,
    "type" TEXT NOT NULL,

    CONSTRAINT "Event_Type_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_typeId_fkey" FOREIGN KEY ("typeId") REFERENCES "Event_Type"("id") ON DELETE SET NULL ON UPDATE CASCADE;
