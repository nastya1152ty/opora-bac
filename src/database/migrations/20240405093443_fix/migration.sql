/*
  Warnings:

  - You are about to drop the column `dataEnd` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `dataStart` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `dataEnd` on the `Task` table. All the data in the column will be lost.
  - Added the required column `dateStart` to the `Event` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Event" DROP COLUMN "dataEnd",
DROP COLUMN "dataStart",
ADD COLUMN     "dateEnd" TIMESTAMP(3),
ADD COLUMN     "dateStart" TIMESTAMP(3) NOT NULL;

-- AlterTable
ALTER TABLE "Task" DROP COLUMN "dataEnd",
ADD COLUMN     "dateEnd" TIMESTAMP(3);
