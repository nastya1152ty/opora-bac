import { UnauthorizedException } from '@nestjs/common';
import * as CryptoJS from 'crypto-js';

export async function compareEncodedPassword(
  inputPassword: string,
  dbPassword: string
) {
  dbPassword = await decodePassword(dbPassword);
  if (inputPassword !== dbPassword) {
    throw new UnauthorizedException();
  }
  return true;
}

export async function encodePassword(password: string) {
  return CryptoJS.AES.encrypt(password, process.env.JWT_SECRET).toString();
}

export async function decodePassword(password: string) {
  const bytes = CryptoJS.AES.decrypt(password, process.env.JWT_SECRET);
  return bytes.toString(CryptoJS.enc.Utf8);
}
